OUTPUT_DIR = output/

all: ING1 

ING1:
	pandoc --toc --chapters -o $(OUTPUT_DIR)/ING1.pdf `find ING1 -name *.rst`

clean:
	rm -rf $(OUTPUT_DIR)

.PHONY: clean

