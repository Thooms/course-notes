#!/usr/bin/env python

import os
import sys

os.system("rm -f Makefile*; touch Makefile")

output_dir = "output/"

if len(sys.argv) > 1:
    output_dir = sys.argv[1]

f = open("Makefile", 'w')

f.write("OUTPUT_DIR = " + output_dir + "\n\n")

f.write("all: ")

for d in filter(os.path.isdir, os.listdir(".")):
    if d[0] != '.':
        f.write(str(d) + " ")

f.write("\n\n")

for d in filter(os.path.isdir, os.listdir(".")):
    if d[0] != '.':
        f.write(str(d) + ":\n")
        f.write("\t" 
                + 'pandoc --toc --chapters -o $(OUTPUT_DIR)/' 
                + str(d) + '.pdf `find ' + str(d) + ' -name *.rst`\n\n')

f.write("clean:\n\trm -rf $(OUTPUT_DIR)\n\n")
f.write(".PHONY: clean\n\n")


        
